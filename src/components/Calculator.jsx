import { useDispatch, useSelector } from 'react-redux';

import {doNothing, addDigit, addDot, flushToStack, sumStack, division, undo, add, subtraction, squareRoot,
  product} from 'state/actions';

import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';

import styles from './Calculator.module.css';

const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();

  const onClick = () => {
    const action = doNothing();
    dispatch(action);
  };

  const onClickNumber = (number) => {
    const action = addDigit(number);
    dispatch(action);
  };

  const onClickDot = () => {
    const action = addDot();
    dispatch(action);
  };

  const onClickIntro = () => {
    const action = flushToStack();
    dispatch(action);
  };

  const onClickDivision = () => {
    const action = division();
    dispatch(action);
  };

  const onClickUndo = () => {
    const action = undo();
    dispatch(action);
  };

  const onClickSum = () => {
    const action = sumStack();
    dispatch(action);
  };

  const onClickAdd = () => {
    const action = add();
    dispatch(action);
  };

  const onClickSubtraction = () => {
    const action = subtraction();
    dispatch(action);
  };

  const onClickSqrt = () => {
    const action = squareRoot();
    dispatch(action);
  };

  const onClickProduct = () => {
    const action = product();
    dispatch(action);
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClickDot()}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClickAdd()}>+</button>
        <button onClick={() => onClickSubtraction()}>-</button>
        <button onClick={() => onClickProduct()}>x</button>
        <button onClick={() => onClickDivision()}>/</button>
        <button onClick={() => onClickSqrt()}>√</button>
        <button onClick={() => onClickSum()}>Σ</button>
        <button onClick={() => onClickUndo()}>Undo</button>
        <button onClick={() => onClickIntro()}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
