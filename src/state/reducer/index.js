import {
  addAction,
  addADigitToCurrentNumberAction,
  addDecimalDotAction,
  divisionAction,
  flushCurrentNumberToStackAction,
  summationStackAction,
  undoAction,
  subtractionAction,
  squareRootAction,
  productAction
} from 'state/actions/actions';

export const rootReducer = (state, action) => {
  if (state === undefined) {
    return {
      displayCurrentNumber: "", currentNumber: null, stack: [], statesHistory: []
    };
  }

  switch (action.type) {
    case 'DO_NOTHING':
      return state;
    case 'ADD_DOT':
      return addDecimalDotAction(state);
    case 'FLUSH_STACK':
      return flushCurrentNumberToStackAction(state);
    case 'ADD_DIGIT':
      return addADigitToCurrentNumberAction(state, action);
    case 'SUM_STACK':
      return summationStackAction(state);
    case 'DIV':
      return divisionAction(state);
    case 'PROD':
      return productAction(state);
    case 'ADD':
      return addAction(state);
    case 'SUBTRACT':
      return subtractionAction(state);
    case 'SQRT':
      return squareRootAction(state);
    case 'UNDO':
      return undoAction(state);
    default:
      return state;
  }
};
