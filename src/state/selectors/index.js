export const selectCurrentNumber = (state) => {
  return state.displayCurrentNumber;
};

export const selectCurrentStack = (state) => {
  return state.stack;
};
