export const addDecimalDotAction = (state) => {
    if (state.displayCurrentNumber.indexOf(".") === -1) {
        return {
            displayCurrentNumber: state.displayCurrentNumber + ".",
            currentNumber: Number(state.currentNumber + "."),
            stack: state.stack,
            statesHistory: [...state.statesHistory, state]
        }
    }
    return state
};

export const flushCurrentNumberToStackAction = (state) => {
    if (state.currentNumber !== "") {
        return {
            displayCurrentNumber: "",
            currentNumber: null,
            stack: [state.currentNumber, ...state.stack],
            statesHistory: [...state.statesHistory, state]
        }
    }
    return state;
};

export const addADigitToCurrentNumberAction = (state, action) => {
    return {
        displayCurrentNumber: state.displayCurrentNumber.concat(action.digit.toString()),
        currentNumber: Number(state.displayCurrentNumber + action.digit),
        stack: state.stack,
        statesHistory: [...state.statesHistory, state]
    };
};

export const summationStackAction = (state) => {
    if (state.currentNumber != null) {
        return {
            displayCurrentNumber: "",
            currentNumber: null,
            stack: [[state.currentNumber, ...state.stack].reduce((a, b) => a + b, 0)],
            statesHistory: [...state.statesHistory, state]
        }
    } else if (state.stack.length !== 0) {
        return {
            displayCurrentNumber: "",
            currentNumber: null,
            stack: [state.stack.reduce((a, b) => a + b, 0)],
            statesHistory: [...state.statesHistory, state]
        }
    }
    return state;
};

export const divisionAction = (state) => {
    let tmpStack = [...state.stack];
    if (state.currentNumber != null) {
        tmpStack.unshift(state.currentNumber);
    }
    if (tmpStack.length >= 2) {
        let dividend = tmpStack.shift();
        let divisor = tmpStack.shift();

        if (divisor !== 0) {
            tmpStack.unshift(dividend/divisor);
            return {
                displayCurrentNumber: "",
                currentNumber: null,
                stack: [...tmpStack],
                decimalLength: 0,
                statesHistory: [...state.statesHistory, state]
            }
        }
    }
    return state;
};

export const undoAction = (state) => {
    if (state.statesHistory.length >= 1) {
        return {
            ...state.statesHistory.pop()
        }
    }
    return state;
};

export const addAction = (state) => {
    let tmpStack = [...state.stack];
    if (state.currentNumber != null) {
        tmpStack.unshift(state.currentNumber);
    }
    if (tmpStack.length >= 2) {
        let element1 = tmpStack.shift();
        let element2 = tmpStack.shift();

        tmpStack.unshift(element1 + element2);
        return {
            displayCurrentNumber: "",
            currentNumber: null,
            stack: [...tmpStack],
            decimalLength: 0,
            statesHistory: [...state.statesHistory, state]
        }
    }
    return state;
};

export const subtractionAction = (state) => {
    let tmpStack = [...state.stack];
    if (state.currentNumber != null) {
        tmpStack.unshift(state.currentNumber);
    }
    if (tmpStack.length >= 2) {
        let element1 = tmpStack.shift();
        let element2 = tmpStack.shift();

        tmpStack.unshift(element1 - element2);
        return {
            displayCurrentNumber: "",
            currentNumber: null,
            stack: [...tmpStack],
            decimalLength: 0,
            statesHistory: [...state.statesHistory, state]
        }
    }
    return state;
};

export const squareRootAction = (state) => {
    let tmpStack = [...state.stack];
    if (state.currentNumber != null) {
        tmpStack.unshift(state.currentNumber);
    }
    if (tmpStack.length >= 1) {
        let element = tmpStack.shift();

        if (element >= 0) {
            tmpStack.unshift(Math.sqrt(element));
            return {
                displayCurrentNumber: "",
                currentNumber: null,
                stack: [...tmpStack],
                decimalLength: 0,
                statesHistory: [...state.statesHistory, state]
            }
        }
    }
    return state;
};

export const productAction = (state) => {
    let tmpStack = [...state.stack];
    if (state.currentNumber != null) {
        tmpStack.unshift(state.currentNumber);
    }
    if (tmpStack.length >= 2) {
        let element1 = tmpStack.shift();
        let element2 = tmpStack.shift();

        tmpStack.unshift(element1 * element2);
        return {
            displayCurrentNumber: "",
            currentNumber: null,
            stack: [...tmpStack],
            decimalLength: 0,
            statesHistory: [...state.statesHistory, state]
        }
    }
    return state;
};
