export const doNothing = () => ({
  type: 'DO_NOTHING',
});

export const addDigit = (digit) => ({
  type: 'ADD_DIGIT', digit
});

export const addDot = () => ({
  type: 'ADD_DOT',
});

export const flushToStack = () => ({
  type: 'FLUSH_STACK',
});

export const sumStack = () => ({
  type: 'SUM_STACK',
});

export const division = () => ({
  type: 'DIV',
});

export const undo = () => ({
  type: 'UNDO',
});

export const add = () => ({
  type: 'ADD',
});

export const subtraction = () => ({
  type: 'SUBTRACT',
});

export const squareRoot = () => ({
  type: 'SQRT',
});

export const product = () => ({
  type: 'PROD',
});